<?php
/**
 * Magic Image Zoom - Magento Extension
 *
 * @package     MagicZoom
 * @category    OnlineBiz
 * @copyright   Copyright 2012 OnlineBiz Inc. (http://www.onlinebizsoft.com)
 * @version:    1.1.3
 */

class OnlineBiz_MagicZoom_Block_Product_View_Media extends Mage_Catalog_Block_Product_View_Media
{
    /** @var OnlineBiz_MagicZoom_Helper_Data */
    protected $_helper;

    /**
     * Retrieve extension helper
     *
     * @return OnlineBiz_MagicZoom_Helper_Data
     */
    public function getLocalHelper()
    {
        if (is_null($this->_helper)) {
            $this->_helper = Mage::helper('onlinebiz_magiczoom');
        }
        return $this->_helper;
    }

    /**
     * @return OnlineBiz_MagicZoom_Block_Product_View_Media
     */
    protected function _beforeToHtml(){
        if ($this->getLocalHelper()->getConfigFlag('enabled')) {
            $this->setTemplate('onlinebizsoft/magic-zoom/catalog/product/view/media.phtml');
        }
        return $this;
    }
}
