<?php
/**
 * Magic Image Zoom - Magento Extension
 *
 * @package     MagicZoom
 * @category    OnlineBiz
 * @copyright   Copyright 2012 OnlineBiz Inc. (http://www.onlinebizsoft.com)
 * @version:    1.1.3
 */

class OnlineBiz_MagicZoom_Model_System_Config_Source_Position
{
    /** @var OnlineBiz_MagicZoom_Helper_Data */
    protected $_helper;

    /**
     * @return OnlineBiz_MagicZoom_Helper_Data
     */
    public function getHelper()
    {
        if (is_null($this->_helper)) {
            $this->_helper = Mage::helper('onlinebiz_magiczoom');
        }
        return $this->_helper;
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        $helper = $this->getHelper();
        return array(
            'right'  => $helper->__('Right'),
            'left'   => $helper->__('Left'),
            'top'    => $helper->__('Top'),
            'bottom' => $helper->__('Bottom'),
            'inside' => $helper->__('Inside')
        );
    }
}
