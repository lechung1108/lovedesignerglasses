<?php
/**
 * Magic Image Zoom - Magento Extension
 *
 * @package     MagicZoom
 * @category    OnlineBiz
 * @copyright   Copyright 2012 OnlineBiz Inc. (http://www.onlinebizsoft.com)
 * @version:    1.1.3
 */

class OnlineBiz_MagicZoom_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * @param string $node
     * @return mixed
     */
    public function getConfigData($node)
    {
        return Mage::getStoreConfig(sprintf('catalog/magiczoom/%s', $node));
    }

    /**
     * @param string $node
     * @return bool
     */
    public function getConfigFlag($node)
    {
        return (bool) Mage::getStoreConfig(sprintf('catalog/magiczoom/%s', $node));
    }
}
