<?php

class OnlineBiz_Themesetting_Model_Observer 
{
	/**
     * Trget is Desktop
     */
    const TARGET_DESKTOP = 'desktop';

    /**
     * Target is Mobile
     */
    const TARGET_MOBILE = 'mobile';
	
    public function controller_action_predispatch($observer) 
	{
		$_helper = Mage::helper('themesetting');
		if(!$_helper->isEnabled())
			return $this;
        $detector = new OnlineBiz_Themesetting_Model_Detector();
		$design = Mage::getDesign();
		if($_helper->isAutoDetect() && $_helper->getTargetPlatform() ==self::TARGET_MOBILE){
			$design->setPackageName($_helper->getMobilePackage());
			$design->setTheme($_helper->getMobileTheme());
		} else {
			$theme = Mage::getModel('themesetting/themesetting')
					->loadByBrowserAndStore($detector->getBrowser(), $detector->getPlatform(),Mage::app()->getStore()->getStoreId());

			if ($theme->getId()) {
				if ($theme->getTemplate()) {
					$package = $_helper->getPackage($theme->getTemplate());
					$templatetheme = $_helper->getTheme($theme->getTemplate());
					if ($package) {
						$design->setPackageName($package);
					}
					if ($templatetheme) {
						$design->setTheme('template', $templatetheme);
					}
				}
				if ($theme->getLayout()) {
					$layouttheme = $_helper->getTheme($theme->getLayout());
					if ($templatetheme) {
						$design->setTheme('layout', $layouttheme);
					}
				}
				if ($theme->getSkin()) {
					$skintheme = $_helper->getTheme($theme->getSkin());
					if ($skintheme) {
						$design->setTheme('skin', $skintheme);
					}
				}
			}
		}
		return $this;
    }

}