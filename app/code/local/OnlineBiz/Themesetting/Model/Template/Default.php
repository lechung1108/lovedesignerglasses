<?php

class OnlineBiz_Themesetting_Model_Template_Default extends Mage_Core_Model_Abstract
{
	const THEME_PACKAGE = 'default';
	const THEME_TEMPLATE = 'default';
	const THEME_LAYOUT = 'default';
	protected $skin = array();
    public function _construct()
    {
        parent::_construct();
		$this->_skin = array("default_red"=>"Default Red","default_blue"=>"Default Blue");
    }
	
	public function getSkin()
	{
		return $this->_skin;
	}
	
	public function isInstalled($theme)
	{
		$installer = new Mage_Eav_Model_Entity_Setup('core_setup');
		return $installer->getConnection()->fetchRow("select * from `" . $installer->getTable('themesetting/installer') . "` where `theme_name` = '" .$theme. "'");
	}
	public function install($theme_skin)
	{
		$installer = new Mage_Eav_Model_Entity_Setup('core_setup');
		$installer->startSetup();
		$theme = explode('_', $theme_skin);
		if(!$this->isInstalled($theme[0])){
			$now = Mage::app()->getLocale()->date()
				->setTimezone(Mage_Core_Model_Locale::DEFAULT_TIMEZONE)
				->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);

			$installer->getConnection()->insert($installer->getTable('cms/block'), array(
				'title' => 'Bestseller',
				'identifier' => 'catalog_bestseller',
				'content' => '{{block type="themesetting/catalog_bestseller" name="catalog.bestseller" template="themesetting/bestseller.phtml"}}',
				'creation_time' => $now,
				'update_time' => $now,
				'is_active' => 1
			));

			$blockId = $installer->getConnection()->lastInsertId();
			$select = $installer->getConnection()->select()
				->from($installer->getTable('core/store'), array('block_id' => new Zend_Db_Expr($blockId), 'store_id'))
				->where('store_id > 0');

			$installer->run($select->insertFromSelect($installer->getTable('cms/block_store')), array('block_id', 'store_id'));
			
			$installer->getConnection()->insert($installer->getTable('themesetting/installer'), array('theme_name'=>$theme[0],'created_time' => $now));
		}
		$installer->setConfigData('design/package/name', self::THEME_PACKAGE);
			
		$installer->setConfigData('design/theme/template', self::THEME_TEMPLATE);
		
		$installer->setConfigData('design/theme/skin', $theme_skin);
		
		$installer->setConfigData('design/theme/layout', self::THEME_LAYOUT);
		$installer->endSetup();
	}
}