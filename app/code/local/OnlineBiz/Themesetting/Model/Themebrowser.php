<?php

class OnlineBiz_Themesetting_Model_Themebrowser extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('themesetting/themebrowser');
    }
	
	public function loadByTheme($themeId,$browser)
	{
		$themebrowser = $this->getCollection()
							->addFieldToFilter('theme_id',$themeId)
							->addFieldToFilter('browser',$browser)
							->getFirstItem();
		$this->setData($themebrowser->getData());
		return $this;
	}
}