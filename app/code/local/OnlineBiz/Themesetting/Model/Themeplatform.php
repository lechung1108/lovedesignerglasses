<?php

class OnlineBiz_Themesetting_Model_Themeplatform extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('themesetting/themeplatform');
    }
	
	public function loadByTheme($themeId,$platform)
	{
		$themeplatform = $this->getCollection()
							->addFieldToFilter('theme_id',$themeId)
							->addFieldToFilter('platform',$platform)
							->getFirstItem();
		$this->setData($themeplatform->getData());
		return $this;
	}
}