<?php

class OnlineBiz_Themesetting_Model_Mysql4_Themebrowser extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
		// Note that the theme_browser_id refers to the key field in your database table.
        $this->_init('themesetting/themebrowser', 'theme_browser_id');
    }
}