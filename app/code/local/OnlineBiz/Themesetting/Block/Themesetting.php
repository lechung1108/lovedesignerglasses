<?php
class OnlineBiz_Themesetting_Block_Themesetting extends Mage_Adminhtml_Block_Template
{
    public function getThemeSetting()     
    {
		$tempDir = BP . DS . 'app' . DS . 'code' . DS . 'local' . DS . 'OnlineBiz' . DS . 'Themesetting' . DS . 'Model' . DS . 'Template';
		$themes = array();
        if (!$this->hasData('themesetting')) {
			$dir = opendir($tempDir);
			while ($file = readdir($dir)) {
				if (eregi("\.php",$file)) { /* Look for files with .php extension */
					$file = str_replace('.php', '', $file);
					$themes = array_merge($themes, Mage::getModel('themesetting/template_'.strtolower($file))->getSkin());
				}
			}
            $this->setData('themesetting', $themes);
        }
        return $this->getData('themesetting');
        
    }
	public function getInstalledThemes()
	{
		if(!$this->hasData('installed_themes')){
			$installedThemes = Mage::getResourceModel('themesetting/themesetting')->getInstalledThemes();
			$this->setData('installed_themes', $installedThemes);
		}
		return $this->getData('installed_themes');
	}
	public function getCurrentTheme()
	{
		return Mage::getStoreConfig('design/theme/skin');
	}
	
}