<?php
class OnlineBiz_Themesetting_Block_Adminhtml_Thememanager extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_thememanager';
    $this->_blockGroup = 'themesetting';
    $this->_headerText = Mage::helper('themesetting')->__('Theme Manager');
    $this->_addButtonLabel = Mage::helper('themesetting')->__('Add New Theme');
    parent::__construct();
  }
}