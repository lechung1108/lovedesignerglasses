<?php

class OnlineBiz_Themesetting_Block_Adminhtml_Thememanager_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('themesetting_form', array('legend'=>Mage::helper('themesetting')->__('Theme information')));
     
      $fieldset->addField('title', 'text', array(
          'label'     => Mage::helper('themesetting')->__('Title'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'title',
      ));	  
      $fieldset->addField('stores', 'multiselect', array(
                'name' => 'stores',
                'label' => Mage::helper('cms')->__('Store View'),
                'title' => Mage::helper('cms')->__('Store View'),
                'required' => true,
                'values' => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(false, true),
            ));
      $fieldset->addField('browser', 'multiselect', array(
          'label'     => Mage::helper('themesetting')->__('Browsers'),
          'class'     => 'required-entry',
          'required'  => true,
		  'values'    => Mage::helper('themesetting')->getBrowserOption(),
          'name'      => 'browser',
      ));		 

      $fieldset->addField('platform', 'multiselect', array(
          'label'     => Mage::helper('themesetting')->__('Platforms'),
          'class'     => 'required-entry',
          'required'  => true,
		  'values'    => Mage::helper('themesetting')->getPlatformOption(),
          'name'      => 'platform',
      ));		  
	  
      $fieldset->addField('template', 'select', array(
          'label'     => Mage::helper('themesetting')->__('Template'),
          'class'     => 'required-entry',
          'required'  => true,
		  'values'    => Mage::helper('themesetting')->getTemplateOption(),
          'name'      => 'template',
      ));	  	  
	  
      $fieldset->addField('skin', 'select', array(
          'label'     => Mage::helper('themesetting')->__('Skin (Images / CSS)'),
          'class'     => 'required-entry',
          'required'  => true,
		  'values'    => Mage::helper('themesetting')->getSkinOption(),
          'name'      => 'skin',
      ));	

      $fieldset->addField('layout', 'select', array(
          'label'     => Mage::helper('themesetting')->__('Layout'),
          'class'     => 'required-entry',
          'required'  => true,
		  'values'    => Mage::helper('themesetting')->getLayoutOption(),
          'name'      => 'layout',
      ));	  
	  
      $fieldset->addField('status', 'select', array(
          'label'     => Mage::helper('themesetting')->__('Status'),
          'name'      => 'status',
		  'required'  => true,
          'values'    => array(
              array(
                  'value'     => 1,
                  'label'     => Mage::helper('themesetting')->__('Enabled'),
              ),

              array(
                  'value'     => 2,
                  'label'     => Mage::helper('themesetting')->__('Disabled'),
              ),
          ),
      ));
     
      $fieldset->addField('description', 'editor', array(
          'name'      => 'description',
          'label'     => Mage::helper('themesetting')->__('Description'),
          'title'     => Mage::helper('themesetting')->__('Description'),
          'style'     => 'width:400px; height:150px;',
          'wysiwyg'   => false,
      ));
     
      if ( Mage::registry('themesetting_data') ) {
          $data = Mage::registry('themesetting_data')->getData();
          if (isset($data['stores']) && $data['stores']){
              $data['stores'] = explode(',',$data['stores']);
          }
          $form->setValues($data);
      }
      return parent::_prepareForm();
  }
}