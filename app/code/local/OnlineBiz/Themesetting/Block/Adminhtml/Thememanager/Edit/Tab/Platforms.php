<?php

class OnlineBiz_Themesetting_Block_Adminhtml_Thememanager_Edit_Tab_Platforms 
	extends Mage_Adminhtml_Block_Template
{
	protected function _prepareLayout()
	{
		parent::_prepareLayout();
		$this->setTemplate('themesetting/platforms.phtml');
	}
}