<?php
/**
 * Extensions Manager Extension
 *
 * PHP versions 4 and 5
 *
 * LICENSE: This source file is subject to version 3.0 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://store.onlinebizsoft.com/license.txt.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to admin@onlinebizsoft.com so we can mail you a copy immediately.
 *
 * @category   Magento Extensions
 * @package    ExtensionManager
 * @author     OnlineBiz <sales@onlinebizsoft.com>
 * @copyright  2007-2011 OnlineBiz
 * @license    http://store.onlinebizsoft.com/license.txt
 * @version    1.0.1
 * @link       http://store.onlinebizsoft.com
 */
 
class OnlineBiz_ObBase_Block_Page_Html_Head extends Mage_Page_Block_Html_Head
{
	 /**
     * Initialize template
     *
     */
    protected function _construct()
    {
        $this->setTemplate('page/html/head.phtml');
		if(Mage::getStoreConfig('obconfig/general/include_jquery')) {
			$this->_data['items']['js/onlinebizsoft/obbase/jquery-1.8.2.min.js'] = array(
				'type'   => 'js',
				'name'   => 'onlinebizsoft/obbase/jquery-1.8.2.min.js',
				'params' => '',
				'if'     => '',
				'cond'   => '',
			);
		}
		if(Mage::getStoreConfig('obconfig/general/include_jquery_noconflict')) {
			$this->_data['items']['js/onlinebizsoft/obbase/no-conflict.js'] = array(
				'type'   => 'js',
				'name'   => 'onlinebizsoft/obbase/no-conflict.js',
				'params' => '',
				'if'     => '',
				'cond'   => '',
			);
		}
    }
	
}
