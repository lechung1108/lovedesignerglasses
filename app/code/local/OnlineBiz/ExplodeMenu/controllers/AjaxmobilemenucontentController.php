<?php

class OnlineBiz_ExplodeMenu_AjaxmobilemenucontentController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        $menu = Mage::helper('explodemenu')->getMobileMenuContent();
        $this->getResponse()->setBody($menu);
    }
}
