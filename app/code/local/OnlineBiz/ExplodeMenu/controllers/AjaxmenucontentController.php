<?php

class OnlineBiz_ExplodeMenu_AjaxmenucontentController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        $menu = Mage::helper('explodemenu')->getMenuContent();
        $this->getResponse()->setBody($menu);
    }
}
