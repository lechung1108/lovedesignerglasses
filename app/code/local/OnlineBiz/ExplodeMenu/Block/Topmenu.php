<?php

if (!Mage::getStoreConfig('explodemenu/general/enabled') ||
   (Mage::getStoreConfig('explodemenu/general/ie6_ignore') && Mage::helper('explodemenu')->isIE6()))
{
    class OnlineBiz_ExplodeMenu_Block_Topmenu extends Mage_Page_Block_Html_Topmenu
    {

    }
    return;
}

class OnlineBiz_ExplodeMenu_Block_Topmenu extends OnlineBiz_ExplodeMenu_Block_Navigation
{

}
