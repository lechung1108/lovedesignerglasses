<?php

class OnlineBiz_ExplodeMenu_Block_Toggle extends Mage_Core_Block_Template
{
    public function _prepareLayout()
    {
        if (!Mage::getStoreConfig('explodemenu/general/enabled')) return;
        if (Mage::getStoreConfig('explodemenu/general/ie6_ignore') && Mage::helper('explodemenu')->isIE6()) return;
        $layout = $this->getLayout();
        $topnav = $layout->getBlock('catalog.topnav');
        if (is_object($topnav)) {
            $topnav->setTemplate('onlinebizsoft/explodemenu/top.phtml');
            $head = $layout->getBlock('head');
            $head->addItem('skin_js', 'js/onlinebizsoft/explodemenu/explodemenu.js');
            $head->addItem('skin_css', 'css/onlinebizsoft/explodemenu/explodemenu.css');
            // --- Insert menu content ---
            if (!Mage::getStoreConfig('explodemenu/general/ajax_load_content')) {
                $menuContent = $layout->getBlock('explodemenu-content');
                if (!is_object($menuContent)) {
                    $menuContent = $layout->createBlock('core/template', 'explodemenu-content')
                        ->setTemplate('onlinebizsoft/explodemenu/menucontent.phtml');
                }
                if (Mage::getStoreConfig('explodemenu/general/move_code_to_bottom')) {
                    $positionTargetName = 'before_body_end';
                } else {
                    $positionTargetName = 'content';
                }
                $positionTarget = $layout->getBlock($positionTargetName);
                if (is_object($positionTarget)) $positionTarget->append($menuContent);
            }
        }
    }
}
