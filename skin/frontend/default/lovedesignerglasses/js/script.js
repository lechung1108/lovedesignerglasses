/**
 * Author: Thompson Le
 * Date: 12/24/2013
 * Time: 15:35 PM
 */


	equalheight = function(container){

	var currentTallest = 0,
		 currentRowStart = 0,
		 rowDivs = new Array(),
		 $el,
		 topPosition = 0;
	 jQuery(container).each(function() {

	   $el = jQuery(this);
	   jQuery($el).height('auto')
	   topPostion = $el.position().top;

	   if (currentRowStart != topPostion) {
		 for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
		   rowDivs[currentDiv].height(currentTallest);
		 }
		 rowDivs.length = 0; // empty the array
		 currentRowStart = topPostion;
		 currentTallest = $el.height();
		 rowDivs.push($el);
	   } else {
		 rowDivs.push($el);
		 currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
	  }
	   for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
		 rowDivs[currentDiv].height(currentTallest);
	   }
	 });
	}

	jQuery(window).load(function() {
		equalheight('.products-grid li.item-inner');
		equalheight('.products-grid li.item-inner .product-name');
		equalheight('.products-grid li.item-inner .price-box');
		equalheight('.products-grid li.item-inner .ratings');
		equalheight('.account-login .col-sm-6');
		equalheight('.account-login .col-sm-6 .content');
	});
	jQuery(window).resize(function(){
		equalheight('.products-grid li.item-inner');
		equalheight('.products-grid li.item-inner .product-name');
		equalheight('.products-grid li.item-inner .price-box');
		equalheight('.products-grid li.item-inner .ratings');
		equalheight('.account-login .col-sm-6');
		equalheight('.account-login .col-sm-6 .content');
	});

	jQuery(function(jQuery) {
		jQuery('.collapsible').each(function(index){
			jQuery(this).prepend('<span class="opener">&nbsp;</span>');
			if (jQuery(this).hasClass('active'))
			{
				jQuery(this).children('.block-content').css('display', 'block');
			}
			else
			{
				jQuery(this).children('.block-content').css('display', 'none');
			}			
		});
		
		jQuery('.collapsible .opener').click(function() {
			
			var parent = jQuery(this).parent();
			if (parent.hasClass('active'))
			{
				jQuery(this).siblings('.block-content').stop(true).slideUp(300);
				parent.removeClass('active');
			}
			else
			{
				jQuery(this).siblings('.block-content').stop(true).slideDown(300);
				parent.addClass('active');
			}
			
		});
	}); 
