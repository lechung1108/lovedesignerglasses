function onlinebizsoftShowMenuPopup(objMenu, event, popupId)
{
    if (typeof onlinebizsoftExplodemenuTimerHide[popupId] != 'undefined') clearTimeout(onlinebizsoftExplodemenuTimerHide[popupId]);
    objMenu = $(objMenu.id); var popup = $(popupId); if (!popup) return;
    if (!!onlinebizsoftActiveMenu) {
        onlinebizsoftHideMenuPopup(objMenu, event, onlinebizsoftActiveMenu.popupId, onlinebizsoftActiveMenu.menuId);
    }
    onlinebizsoftActiveMenu = {menuId: objMenu.id, popupId: popupId};
    if (!objMenu.hasClassName('active')) {
        onlinebizsoftExplodemenuTimerShow[popupId] = setTimeout(function() {
            objMenu.addClassName('active');
            var popupWidth = EXPLODEMENU_POPUP_WIDTH;
            if (!popupWidth) popupWidth = popup.getWidth();
            var pos = onlinebizsoftPopupPos(objMenu, popupWidth);
            popup.style.top = pos.top + 'px';
            popup.style.left = pos.left + 'px';
            onlinebizsoftSetPopupZIndex(popup);
            if (EXPLODEMENU_POPUP_WIDTH)
                popup.style.width = EXPLODEMENU_POPUP_WIDTH + 'px';
            // --- Static Block width ---
            var block2 = $(popupId).select('div.block2');
            if (typeof block2[0] != 'undefined') {
                var wStart = block2[0].id.indexOf('_w');
                if (wStart > -1) {
                    var w = block2[0].id.substr(wStart+2);
                } else {
                    var w = 0;
                    $(popupId).select('div.block1 div.column').each(function(item) {
                        w += $(item).getWidth();
                    });
                }
                if (w) block2[0].style.width = w + 'px';
            }
            // --- change href ---
            var onlinebizsoftMenuAnchor = $(objMenu.select('a')[0]);
            onlinebizsoftChangeTopMenuHref(onlinebizsoftMenuAnchor, true);
            // --- show popup ---
            popup.style.display = 'block';
            //jQuery('#' + popupId).stop(true, true).fadeIn();
        }, EXPLODEMENU_POPUP_DELAY_BEFORE_DISPLAYING);
    }
}

function onlinebizsoftHideMenuPopup(element, event, popupId, menuId)
{
    if (typeof onlinebizsoftExplodemenuTimerShow[popupId] != 'undefined') clearTimeout(onlinebizsoftExplodemenuTimerShow[popupId]);
    var element = $(element); var objMenu = $(menuId) ;var popup = $(popupId); if (!popup) return;
    var onlinebizsoftCurrentMouseTarget = getCurrentMouseTarget(event);
    if (!!onlinebizsoftCurrentMouseTarget) {
        if (!onlinebizsoftIsChildOf(element, onlinebizsoftCurrentMouseTarget) && element != onlinebizsoftCurrentMouseTarget) {
            if (!onlinebizsoftIsChildOf(popup, onlinebizsoftCurrentMouseTarget) && popup != onlinebizsoftCurrentMouseTarget) {
                if (objMenu.hasClassName('active')) {
                    onlinebizsoftExplodemenuTimerHide[popupId] = setTimeout(function() {
                        objMenu.removeClassName('active');
                        // --- change href ---
                        var onlinebizsoftMenuAnchor = $(objMenu.select('a')[0]);
                        onlinebizsoftChangeTopMenuHref(onlinebizsoftMenuAnchor, false);
                        // --- hide popup ---
                        popup.style.display = 'none';
                        //jQuery('#' + popupId).stop(true, true).fadeOut();
                    }, EXPLODEMENU_POPUP_DELAY_BEFORE_HIDING);
                }
            }
        }
    }
}

function onlinebizsoftPopupOver(element, event, popupId, menuId)
{
    if (typeof onlinebizsoftExplodemenuTimerHide[popupId] != 'undefined') clearTimeout(onlinebizsoftExplodemenuTimerHide[popupId]);
}

function onlinebizsoftPopupPos(objMenu, w)
{
    var pos = objMenu.cumulativeOffset();
    var wraper = $('explodemenu');
    var posWraper = wraper.cumulativeOffset();
    var xTop = pos.top - posWraper.top
    if (EXPLODEMENU_POPUP_TOP_OFFSET) {
        xTop += EXPLODEMENU_POPUP_TOP_OFFSET;
    } else {
        xTop += objMenu.getHeight();
    }
    var res = {'top': xTop};
    if (EXPLODEMENU_RTL_MODE) {
        var xLeft = pos.left - posWraper.left - w + objMenu.getWidth();
        if (xLeft < 0) xLeft = 0;
        res.left = xLeft;
    } else {
        var wWraper = wraper.getWidth();
        var xLeft = pos.left - posWraper.left;
        if ((xLeft + w) > wWraper) xLeft = wWraper - w;
        if (xLeft < 0) xLeft = 0;
        res.left = xLeft;
    }
    return res;
}

function onlinebizsoftChangeTopMenuHref(onlinebizsoftMenuAnchor, state)
{
    if (state) {
        onlinebizsoftMenuAnchor.href = onlinebizsoftMenuAnchor.rel;
    } else if (onlinebizsoftIsMobile.any()) {
        onlinebizsoftMenuAnchor.href = 'javascript:void(0);';
    }
}

function onlinebizsoftIsChildOf(parent, child)
{
    if (child != null) {
        while (child.parentNode) {
            if ((child = child.parentNode) == parent) {
                return true;
            }
        }
    }
    return false;
}

function onlinebizsoftSetPopupZIndex(popup)
{
    $$('.onlinebizsoft-explodemenu-popup').each(function(item){
       item.style.zIndex = '9999';
    });
    popup.style.zIndex = '10000';
}

function getCurrentMouseTarget(xEvent)
{
    var onlinebizsoftCurrentMouseTarget = null;
    if (xEvent.toElement) {
        onlinebizsoftCurrentMouseTarget = xEvent.toElement;
    } else if (xEvent.relatedTarget) {
        onlinebizsoftCurrentMouseTarget = xEvent.relatedTarget;
    }
    return onlinebizsoftCurrentMouseTarget;
}

function getCurrentMouseTargetMobile(xEvent)
{
    if (!xEvent) var xEvent = window.event;
    var onlinebizsoftCurrentMouseTarget = null;
    if (xEvent.target) onlinebizsoftCurrentMouseTarget = xEvent.target;
        else if (xEvent.srcElement) onlinebizsoftCurrentMouseTarget = xEvent.srcElement;
    if (onlinebizsoftCurrentMouseTarget.nodeType == 3) // defeat Safari bug
        onlinebizsoftCurrentMouseTarget = onlinebizsoftCurrentMouseTarget.parentNode;
    return onlinebizsoftCurrentMouseTarget;
}

/* Mobile */
function onlinebizsoftMenuButtonToggle()
{
    $('menu-content').toggle();
}

function onlinebizsoftGetMobileSubMenuLevel(id)
{
    var rel = $(id).readAttribute('rel');
    return parseInt(rel.replace('level', ''));
}

function onlinebizsoftSubMenuToggle(obj, activeMenuId, activeSubMenuId)
{
    var currLevel = onlinebizsoftGetMobileSubMenuLevel(activeSubMenuId);
    // --- hide submenus ---
    $$('.onlinebizsoft-explodemenu-submenu').each(function(item) {
        if (item.id == activeSubMenuId) return;
        var xLevel = onlinebizsoftGetMobileSubMenuLevel(item.id);
        if (xLevel >= currLevel) {
            $(item).hide();
        }
    });
    // --- reset button state ---
    $('explodemenu-mobile').select('span.button').each(function(xItem) {
        var subMenuId = $(xItem).readAttribute('rel');
        if (!$(subMenuId).visible()) {
            $(xItem).removeClassName('open');
        }
    });
    // ---
    if ($(activeSubMenuId).getStyle('display') == 'none') {
        $(activeSubMenuId).show();
        $(obj).addClassName('open');
    } else {
        $(activeSubMenuId).hide();
        $(obj).removeClassName('open');
    }
}

function onlinebizsoftResetMobileMenuState()
{
    $('menu-content').hide();
    $$('.onlinebizsoft-explodemenu-submenu').each(function(item) {
        $(item).hide();
    });
    $('explodemenu-mobile').select('span.button').each(function(item) {
        $(item).removeClassName('open');
    });
}

function onlinebizsoftExplodeMenuMobileToggle()
{
    var w = window,
        d = document,
        e = d.documentElement,
        g = d.getElementsByTagName('body')[0],
        x = w.innerWidth || e.clientWidth || g.clientWidth,
        y = w.innerHeight|| e.clientHeight|| g.clientHeight;

    if ((x < 800 || onlinebizsoftIsMobile.any()) && onlinebizsoftMobileMenuEnabled) {
        $('explodemenu').hide();
        $('explodemenu-mobile').show();
        // --- ajax load ---
        if (onlinebizsoftMoblieMenuAjaxUrl) {
            new Ajax.Updater({success: 'menu-content'}, onlinebizsoftMoblieMenuAjaxUrl, {
                method: 'get',
                asynchronous: true
            });
            onlinebizsoftMoblieMenuAjaxUrl = null;
        }
    } else {
        $('explodemenu-mobile').hide();
        onlinebizsoftResetMobileMenuState();
        $('explodemenu').show();
        // --- ajax load ---
        if (onlinebizsoftMenuAjaxUrl) {
            new Ajax.Updater({success: 'explodemenu'}, onlinebizsoftMenuAjaxUrl, {
                method: 'get',
                asynchronous: true
            });
            onlinebizsoftMenuAjaxUrl = null;
        }
    }

    if ($('explodemenu-loading')) $('explodemenu-loading').remove();
}

var onlinebizsoftIsMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (onlinebizsoftIsMobile.Android() || onlinebizsoftIsMobile.BlackBerry() || onlinebizsoftIsMobile.iOS() || onlinebizsoftIsMobile.Opera() || onlinebizsoftIsMobile.Windows());
    }
};
